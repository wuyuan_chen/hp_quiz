const url =
    "https://www.potterapi.com/v1/characters?key=$2a$10$wzufhjFDQz8Ti43BnZtFju3zsekN4lJsVET3b2ICBjzSe8TmSkJo.";

let charData;
fetch(url)
    .then((response) => response.json())
    .then((data) => {
        // sort the data by character name in lexicographical order
        const sortedData = data.sort(function (a, b) {
            const x = a.name.toLowerCase(),
                y = b.name.toLowerCase();

            return x < y ? -1 : x > y ? 1 : 0;
        });

        sortedData.forEach((charInfo) => {
            createCharInfoDisplay(charInfo);
        });
    });

/**
 * creates the buttons out of all the character names
 */
function createCharInfoDisplay(charInfo) {
    const charInfoDisp = document.getElementById("char-name-list");
    // create the div to wrap the button and the hidden content
    let charContent = document.createElement("div");
    charContent.className = "char-content";

    //create the text button
    let textButton = document.createElement("button");
    textButton.className = "text-button";
    textButton.innerHTML = charInfo.name;

    // create the hidden content
    let infoDisp = document.createElement("div");
    infoDisp.className = "hidden-info-disp";
    infoDisp.style.display = "none";
    addTable(charInfo, infoDisp);

    // click the button to show the content
    textButton.onclick = function () {
        if (infoDisp.style.display === "none") {
            infoDisp.style.display = "block";
        } else {
            infoDisp.style.display = "none";
        }
    };

    // append elements to html
    charInfoDisp.appendChild(charContent);
    charContent.appendChild(textButton);
    charContent.appendChild(infoDisp);
}
function addTable(charInfo, infoDisp) {
    let c, r, t, header;
    t = document.createElement("table");
    t.className = "char-table";
    t.border = 1;
    const keys = Object.keys(charInfo);

    const charDetail = keys.filter((key) => !key.includes("_"));
    const cols = charDetail.length;
    let headerIdx = 0;

    //Add the header row.
    let tr = t.insertRow(-1);
    tr.className = "char-thead-tr";
    for (let i = 0; i < cols; i++) {
        let key = charDetail[i];
        const headerCell = document.createElement("th");
        headerCell.innerHTML = "<b>" + key + "</b>";
        tr.appendChild(headerCell);
    }

    //add the data rows
    tr = t.insertRow(-1);
    tr.className = "char-tbody-tr";
    for (let i = 0; i < cols; i++) {
        let key = charDetail[i];
        let cell = tr.insertCell(-1);
        cell.innerHTML = charInfo[key];
    }

    infoDisp.appendChild(t);
}
