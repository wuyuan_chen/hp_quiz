filePath = "../header.html";

fetch(filePath)
    .then(function (response) {
        return response.text();
    })
    .then(function (body) {
        // console.log(body);
        document.querySelector(".show-header").innerHTML = body;
    });

// const loadHtml = function (parentElementId, filePath) {
//     const init = {
//         method: "GET",
//         headers: { "Content-Type": "text/html" },
//         mode: "cors",
//         cache: "default",
//     };
//     const req = new Request(filePath, init);
//     fetch(req)
//         .then(function (response) {
//             return response.text();
//         })
//         .then(function (body) {
//             // Replace `#` char in case the function gets called `querySelector` or jQuery style
//             if (parentElementId.startsWith("#")) {
//                 parentElementId.replace("#", "");
//             }
//             document.getElementById(parentElementId).innerHTML = body;
//         });
// };

// loadHtml(".body", "../header.html");
