const url =
    "https://www.potterapi.com/v1/spells?key=$2a$10$wzufhjFDQz8Ti43BnZtFju3zsekN4lJsVET3b2ICBjzSe8TmSkJo.";

let charData;
fetch(url)
    .then((response) => response.json())
    .then((data) => {
        console.log(data);
        createSpellTable(data);
    });

function createSpellTable(data) {
    let c, r, t, header;
    let spellContainer = document.getElementById("spell-container-id");
    t = document.createElement("table");
    // t.border = 1;
    //Add the header row.
    const headerInfo = ["Spell", "Type", "Effect"];
    let tr = t.insertRow(-1);
    tr.className = "spell-thead-tr";
    for (let i = 0; i < headerInfo.length; i++) {
        let key = headerInfo[i];
        const headerCell = document.createElement("th");
        headerCell.innerHTML = "<b>" + key + "</b>";
        tr.appendChild(headerCell);
    }

    // add the table body

    keys = headerInfo.map((info) => info.toLowerCase());
    for (let i = 0; i < data.length; i++) {
        tr = t.insertRow(-1);
        tr.className = "spell-tbody-tr";
        keys.forEach((key) => {
            console.log();
            let cell = tr.insertCell(-1);
            cell.innerHTML = data[i][key];
        });
    }
    spellContainer.appendChild(t);
}
